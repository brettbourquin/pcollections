package org.pcollections;

import java.io.Serializable;
import java.util.AbstractSet;

public abstract class AbstractSerializableSet<V> extends AbstractSet<V> implements Serializable {
}
